package com.eform.media.service.mediaService;

import com.eform.media.entity.mediaEntity.Image;

import java.util.List;

public interface ImageService {
    public List<Image> getAllImages();

}
