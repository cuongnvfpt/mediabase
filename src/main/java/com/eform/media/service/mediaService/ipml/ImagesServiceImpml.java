package com.eform.media.service.mediaService.ipml;

import com.eform.media.entity.mediaEntity.Image;
import com.eform.media.repository.mediaRespository.ImageRespository;
import com.eform.media.service.mediaService.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImagesServiceImpml implements ImageService {
    @Autowired
    ImageRespository imageRespository;


    @Override
    public List<Image> getAllImages() {
        return imageRespository.getAllImage();
    }


}
