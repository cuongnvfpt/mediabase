package com.eform.media.controller.mediaAPI;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/mediaAPI")
public class RestUploadController {

    private final Logger logger = LoggerFactory.getLogger(RestUploadController.class);

    //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "E://temp1//";
    private String uploadedFileName="";
    // 3.1.2 Multiple file upload
    @PostMapping("/api/upload/multi")
    public ResponseEntity<?> uploadFileMulti(
            @RequestParam("files") MultipartFile[] uploadfiles) {

        logger.debug("Multiple file upload!");

        // Get file name
         uploadedFileName = Arrays.stream(uploadfiles).map(x -> x.getOriginalFilename())
                .filter(x -> !StringUtils.isEmpty(x)).collect(Collectors.joining(" , "));

        if (StringUtils.isEmpty(uploadedFileName)) {
            return new ResponseEntity("error", HttpStatus.OK);
        }

        try {

            saveUploadedFiles(Arrays.asList(uploadfiles));

        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }


        ResponseEntity rs = new ResponseEntity("/temp1/"
                + uploadedFileName, HttpStatus.OK);

        return rs;

    }


    @DeleteMapping(value= "/delete")
    public ResponseEntity<String> deleteFile(@PathVariable("img") String img) {
        File myObj = new File("E:\\temp1\\"+ img);
        String response="";
        if (myObj.delete()) {
             response = "deleted successfully.";
        } else {
            response = "deleted fail.";
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    //save file
    private void saveUploadedFiles(List<MultipartFile> files) throws IOException {

        for (MultipartFile file : files) {

            if (file.isEmpty()) {
                continue; //next pls
            }

            byte[] bytes = file.getBytes();


                String newName = getNewFileName(file.getOriginalFilename());
                uploadedFileName  = newName;
                Path path = Paths.get(UPLOADED_FOLDER + newName);
                Files.write(path, bytes);

        }

    }
    public static String getNewFileName(String filename) throws IOException {
        File aFile = new File(UPLOADED_FOLDER+filename);
        int fileNo = 0;
        String newFileName = "";
        if (aFile.exists() && !aFile.isDirectory()) {
            //newFileName = filename.replaceAll(getFileExtension(filename), "(" + fileNo + ")" + getFileExtension(filename));

            while(aFile.exists()){
                fileNo++;
                aFile = new File(UPLOADED_FOLDER+"(" + fileNo + ")"+filename);
                newFileName = "(" + fileNo + ")"+filename;
            }


        } else if (!aFile.exists()) {
            aFile.createNewFile();
            
            newFileName = filename;
        }
        return newFileName;
    }
}