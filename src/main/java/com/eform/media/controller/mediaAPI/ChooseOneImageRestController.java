package com.eform.media.controller.mediaAPI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChooseOneImageRestController {
    private final Logger logger = LoggerFactory.getLogger(RestUploadController.class);
    String path;

@GetMapping("/api/loadOneImage")
public String loadImage(){
    return path;
}

@PostMapping("/api/loadOneImage/{name}")
    public String uploadFileMulti(
        @PathVariable("name") String name){
    String rs = "temp/" + name;
    path= rs;
    return rs;
}
}
