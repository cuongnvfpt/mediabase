package com.eform.media.controller.mediaAPI;

import com.eform.media.entity.mediaEntity.Image;
import com.eform.media.service.mediaService.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/mediaAPI")
public class ImageRestController {
    @Autowired
    ImageService imageService;
    @GetMapping("/api/images")
    public List<Image> getAllIMG(){
        return imageService.getAllImages();

    }
}
