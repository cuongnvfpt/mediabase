package com.eform.media.repository.mediaRespository;


import com.eform.media.entity.mediaEntity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRespository extends JpaRepository<Image,String> {
    @Query("SELECT new Image(i.id , i.name , i.path) FROM Image i")
    public List<Image> getAllImage();

}
